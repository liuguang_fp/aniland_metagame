﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ElementType
{
    Tile = 0,
    Building = 1
}
