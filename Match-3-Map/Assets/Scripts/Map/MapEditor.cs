﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public class MapEditor : MonoBehaviour
{
    [HideInInspector]
    public int col; //tile最大列数 
    [HideInInspector]
    public int row;//tile最大行数 


    //菱形短对角线长
    [HideInInspector]
    public float diagonal;
    //菱形边长
    public float side
    {
        get { return diagonal / Mathf.Cos(tileRad); }
    }

    [HideInInspector]
    public GridLayer gridLayer;

    [HideInInspector]
    public int availableMode;

    //显示当前所有Tile的可用状态 
    [HideInInspector]
    public bool showAvailable;

    public static GameObject CurrentSelectObj;

    //当前摄像机俯视角度
    private static int angle = 30;
    //根据摄像机视角换算后在2D平面上每个Tile菱形
    //以长对角线分割后每个等腰三角形的锐角对应的弧度
    public static float tileRad
    {
        get { return Mathf.Atan(Mathf.Sin(angle * Mathf.Deg2Rad)); }
    }

    public object DirectionInfo { get; private set; }

    private void Start()
    {

    }

#if UNITY_EDITOR


    private void OnDrawGizmos()
    {

        Event e = Event.current;
#if UNITY_EDITOR
        if (CurrentSelectObj != null)
        {
            Gizmos.color = Color.yellow;
            MapElement element = CurrentSelectObj.GetComponent<MapElement>();
            Vector2 pos = HandleUtility.GUIPointToWorldRay(e.mousePosition).origin;
            int index = gridLayer.GetGridIndexByPos(pos);
            for (int i = 0; i < element.rowSize; i++)
            {
                for (int j = 0; j < element.colSize; j++)
                {
                    index += j;
                    if (index >= 0 && index < gridLayer.Count)
                    {
                        GridElement grid = gridLayer[index];
                        Gizmos.DrawCube(grid.pivot, new Vector3(0.2f, 0.2f, 0.1f));
                    }
                }
                index += col - 1;
            }
        }
        if (e.alt)
            Selection.activeGameObject = gameObject;
        if (e.shift)
            showAvailable = !showAvailable;
#endif
        Gizmos.color = Color.cyan;
        for (int i = 0; i < col; i++)
        {
            Gizmos.DrawLine(gridLayer[i].bottom, gridLayer[gridLayer.datas.Count - col + i].left);
            if (i == col - 1)
                Gizmos.DrawLine(gridLayer[i].right, gridLayer[gridLayer.datas.Count - col + i].top);
        }
        for (int i = 0; i < col * row; i += col)
        {
            Gizmos.DrawLine(gridLayer[i].bottom, gridLayer[i + col - 1].right);
            if (i == (row - 1) * col)
                Gizmos.DrawLine(gridLayer[i].left, gridLayer[i + col - 1].top);
        }

        if (showAvailable)
            foreach (GridElement item in gridLayer)
            {
                Gizmos.color = item.available ? Color.green : Color.red;
                Gizmos.DrawCube(item.pivot, new Vector3(0.5f, 0.5f, 0.1f));
            }


    }


    public void CreateTile(GridElement data)
    {
        if (CurrentSelectObj == null) return;
        ElementType type = CurrentSelectObj.GetComponent<MapElement>().type;
        switch (type)
        {
            case ElementType.Tile:
                if (data.hasTile) return;
                GameObject objTile = (GameObject)PrefabUtility.InstantiatePrefab(CurrentSelectObj);
                MapElement elementTile = objTile.GetComponent<MapElement>();
                elementTile.SetLayer(data.tileLayer);
                objTile.transform.SetParent(GameObject.Find("TileContent").transform);
                objTile.transform.position = data.pivot;
                data.tile = elementTile;
                elementTile.gridData = data;
                data.hasTile = true;
                break;
            case ElementType.Building:
                if (data.hasBuilding) return;
                GameObject objBuilding = (GameObject)PrefabUtility.InstantiatePrefab(CurrentSelectObj);
                MapElement elementBuilding = objBuilding.GetComponent<MapElement>();
                elementBuilding.SetLayer(data.buildingLayer);
                objBuilding.transform.SetParent(GameObject.Find("TileContent").transform);
                objBuilding.transform.position = new Vector3(data.pivot.x, data.pivot.y, -0.1f);
                data.building = elementBuilding;
                elementBuilding.gridData = data;
                data.hasBuilding = true;

                break;
        }

        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());

    }
    public void SerializeMapData()
    {
        string path = Application.streamingAssetsPath + "/MapData";
        DirectoryInfo direction = new DirectoryInfo(path);
        if (!direction.Exists)
            direction.Create();

        MapManifest manifest = new MapManifest(col, row, diagonal, side);
        for (int i = 0; i < gridLayer.Count; i++)
        {
            GridElement element = gridLayer[i];
            GridElementData data = new GridElementData(element.pivot, element.available, element.tileLayer, element.buildingLayer);
            if (element.hasTile)
            {
                MapElementData tileData = new MapElementData();
                tileData.assetsPath = element.tile.assetsPath;
                tileData.sortingLayer = element.tile.renderer.sortingOrder;
                data.tileData = tileData;
            }
            if (element.hasBuilding)
            {
                MapElementData buildingData = new MapElementData();
                buildingData.assetsPath = element.building.assetsPath;
                buildingData.sortingLayer = element.building.renderer.sortingOrder;
                buildingData.offset_x = element.building.transform.position.x - element.pivot.x;
                buildingData.offset_y = element.building.transform.position.y - element.pivot.y;
                buildingData.offset_z = element.building.transform.position.z - element.pivot.z;
                data.buildingData = buildingData;
            }
            manifest.AddData(data);
        }
        BinarySerialize serialize = new BinarySerialize();
        serialize.SerializeToFile<MapManifest>(path + "/MapData", manifest);
        EditorUtility.DisplayDialog("成功", "导出完成", "确认");

    }
    public void DeleteTile(GridElement data, ElementType type)
    {
        switch (type)
        {
            case ElementType.Tile:
                if (data.hasTile)
                {
                    DestroyImmediate(data.tile.gameObject);
                    data.hasTile = false;
                    data.tile = null;
                }
                break;
            case ElementType.Building:
                if (data.hasBuilding)
                {
                    DestroyImmediate(data.building.gameObject);
                    data.hasBuilding = false;
                    data.building = null;
                }
                break;
        }

        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }
#endif



}





