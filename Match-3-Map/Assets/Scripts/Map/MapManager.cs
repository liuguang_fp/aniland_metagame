﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{

    Transform content;
    GridLayer layer;

    void Start()
    {
        content = GameObject.Find("TileContent").transform;
        StartCoroutine(LoadMap());
    }


    public IEnumerator LoadMap()
    {
        WWW www = new WWW(Application.streamingAssetsPath + "/MapData/MapData");
        yield return www;

        BinarySerialize serialize = new BinarySerialize();
        MapManifest manifest = serialize.DeserializeFromMemory<MapManifest>(www.bytes);
        InitMap(manifest);
    }

    public void InitMap(MapManifest manifest)
    {
        layer = new GridLayer(manifest.gridCol, manifest.gridRow, manifest.gridSide, manifest.gridDiagonal);
        for (int i = 0; i < manifest.Count; i++)
        {
            GridElementData data = manifest[i];
            GridElement grid = new GridElement(manifest.gridDiagonal, manifest.gridSide,
                new Vector3(data.position_x, data.position_y, data.position_z), data.tileLayer,data.buildingLayer);
            if (data.tileData != null)
            {
                GameObject tilePrefab = Resources.Load<GameObject>(data.tileData.assetsPath);
                MapElement tile = Instantiate(tilePrefab, content).GetComponent<MapElement>();
                tile.transform.position = new Vector3(data.position_x, data.position_y, data.position_z);
                tile.SetLayer(data.tileData.sortingLayer);
                grid.tile = tile;
                tile.gridData = grid;
            }
            if (data.buildingData != null)
            {
                GameObject buildingPrefab = Resources.Load<GameObject>(data.buildingData.assetsPath);
                MapElement building = Instantiate(buildingPrefab, content).GetComponent<MapElement>();
                building.transform.position = new Vector3(data.position_x + data.buildingData.offset_x,
                    data.position_y + data.buildingData.offset_y, data.position_z + data.buildingData.offset_z);
                building.SetLayer(data.buildingData.sortingLayer);
                grid.building = building;
                building.gridData = grid;
            }
            layer.AddGridElement(grid);
        }
    }

}
