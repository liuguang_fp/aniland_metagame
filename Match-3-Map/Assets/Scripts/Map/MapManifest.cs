﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MapManifest : IEnumerable
{

    public int gridCol;
    public int gridRow;
    public float gridDiagonal;
    public float gridSide;


    public List<GridElementData> gridDatas;


    public GridElementData this[int index]
    {
        get
        {
            return gridDatas[index];
        }
    }

    public int Count
    {
        get
        {
            return gridDatas.Count;
        }
    }

    public MapManifest(int col, int row, float diagonal, float side)
    {
        gridCol = col;
        gridRow = row;
        gridDiagonal = diagonal;
        gridSide = side;
        gridDatas = new List<GridElementData>(gridCol * gridRow);
    }

    public void AddData(GridElementData data)
    {
        gridDatas.Add(data);
    }


    public IEnumerator GetEnumerator()
    {
        foreach (var item in gridDatas)
        {
            yield return item;
        }
    }
}
