﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class CameraController : MonoBehaviour
{
    public enum TouchType
    {
        None,
        Single,
        Double
    }
    public Text touchCount;

    private MapEditor map;
    private Camera camera;

    public float springRange = 1;
    public float limitRange = 0;
    //拖拽场景时的移动速度
    public float moveSpeed = 2f;
    public float throwSpeed = 1;

    public float scaleSpeed = 1;
    public float minScale = 3;
    public float maxScale = 5;
    public float scaleSpringRange = 1f;

    //用于计算的正切值 
    private float tangent;

    private float throwSpeed_x;
    private float throwSpeed_y;

    private float CameraSize
    {
        get
        {
            return camera.orthographicSize;
        }
        set
        {
            camera.orthographicSize = value;
        }
    }

    Touch touch1;
    Touch touch2;

    //手指触摸屏幕时的世界坐标
    Vector3 mouseDownPos;
    //手指离开屏幕时的世界坐标
    Vector3 mouseUpPos;
    //手指拖动屏幕时上一帧手指的世界坐标
    Vector3 mouseLastPos;

    Vector3 deltaPos;



    float lastDistance;


    TouchType touchMode;

    IEnumerator throwCamera;
    IEnumerator scaleCamera;

    bool beginJudgeMoveSpring;
    bool beginJudgeScaleSpring;


    private void Start()
    {
        map = FindObjectOfType<MapEditor>();
        camera = GetComponent<Camera>();
        tangent = Mathf.Tan(MapEditor.tileRad);


    }

    private void Update()
    {

#if UNITY_EDITOR
        EditorControl();
#elif UNITY_ANDROID
        RealControl();
#endif

        //SpringMove();
    }


    private void EditorControl()
    {
        float scale = Input.GetAxis("Mouse ScrollWheel");
        float targetSize = ScaleLimit(camera.orthographicSize - scale);
        camera.orthographicSize = targetSize;


        if (Input.GetMouseButtonDown(0))
        {
            beginJudgeMoveSpring = false;
            if (throwCamera != null)
                StopCoroutine(throwCamera);
            mouseDownPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        }
        if (Input.GetMouseButtonUp(0))
        {
            //mouseUpPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //Vector3 delta = (mouseUpPos - mouseDownPos);
            //Vector3 targetPos = MoveLimit(transform.position - delta);

            //Debug.LogError(map.gridLayer.TransCoordinate(deltaPos));

            throwCamera = ThrowCamera(deltaPos / 2 * throwSpeed);
            StartCoroutine(throwCamera);

            mouseLastPos = Vector3.zero;
            beginJudgeMoveSpring = true;
        }

        if (Input.GetMouseButton(0))
        {
            if (mouseLastPos != Vector3.zero)
            {
                float speed = JudgeMoveSpring(springRange) ? moveSpeed / 3 : moveSpeed;
                deltaPos = Camera.main.ScreenToWorldPoint(Input.mousePosition)
                    - mouseLastPos;
                Vector3 targetPos = MoveLimit(transform.position - deltaPos * speed);
                transform.position = targetPos;
            }
            mouseLastPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }


        if (beginJudgeMoveSpring)
            SpringMove();

    }

    private void RealControl()
    {

        switch (touchMode)
        {
            case TouchType.None:
            case TouchType.Single:
                if (Input.touchCount == 1)
                    touchMode = TouchType.Single;
                else if (Input.touchCount == 2)
                    touchMode = TouchType.Double;

                break;
            case TouchType.Double:
                if (Input.touchCount == 0)
                {
                    mouseDownPos = Vector3.zero;
                    mouseUpPos = Vector3.zero;
                    mouseLastPos = Vector3.zero;
                    touchMode = TouchType.None;
                }
                else if (Input.touchCount == 1)
                    touchMode = TouchType.Single;
                break;
        }

        if (beginJudgeMoveSpring)
            SpringMove();
        if (beginJudgeScaleSpring)
            SpringScale();
        touchCount.text = beginJudgeMoveSpring.ToString();

        if (touchMode == TouchType.Single)
        {
            touch1 = Input.GetTouch(0);
            if (touch1.phase == TouchPhase.Began)
            {
                if (throwCamera != null)
                    StopCoroutine(throwCamera);

                mouseDownPos = TransPos(touch1.position);
            }
            if (touch1.phase == TouchPhase.Ended)
            {
                throwCamera = ThrowCamera(deltaPos / 2 * throwSpeed);
                StartCoroutine(throwCamera);

                mouseLastPos = Vector3.zero;
                beginJudgeMoveSpring = true;
            }

            if (touch1.phase == TouchPhase.Moved)
            {

                if (mouseLastPos != Vector3.zero)
                {
                    deltaPos = TransPos(touch1.position)
                      - mouseLastPos; /*Input.GetTouch(0).deltaPosition;*/
                    Vector3 targetPos = MoveLimit(transform.position - deltaPos * moveSpeed);
                    transform.position = targetPos;
                }
                mouseLastPos = TransPos(touch1.position);

            }
        }
        else if (touchMode == TouchType.Double)
        {
            //touchCount.text = touch1.phase.ToString() + ":" + touch2.phase.ToString();
            touch1 = Input.GetTouch(0);
            touch2 = Input.GetTouch(1);
            if (touch2.phase == TouchPhase.Began)
            {
                beginJudgeScaleSpring = false;
                if (scaleCamera != null)
                    StopCoroutine(scaleCamera);
            }
            if (touch1.phase == TouchPhase.Ended || touch2.phase == TouchPhase.Ended)
            {
                lastDistance = 0;
                beginJudgeScaleSpring = true;
            }

            if (touch1.phase == TouchPhase.Moved && touch2.phase == TouchPhase.Moved)
            {
                float distance = Vector3.Distance(touch1.position, touch2.position);
                if (lastDistance != 0)
                {
                    float speed = JudgeMoveSpring(springRange) ? moveSpeed / 3 : moveSpeed;
                    float size = CameraSize - (distance - lastDistance) * 0.001f * speed;
                    float targetSize = ScaleLimit(size);
                    CameraSize = targetSize;
                }
                lastDistance = distance;
            }
        }
    }



    private IEnumerator ScaleSpring()
    {
        while (JudgeScaleSpring())
        {
            if (camera.orthographicSize < 3)
            {
                camera.orthographicSize += 0.1f;
            }
            else if (camera.orthographicSize > 5)
            {
                camera.orthographicSize -= 0.1f;
            }
            yield return null;
        }
    }



    /// <summary>
    /// 将屏幕抛出效果
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    private IEnumerator ThrowCamera(Vector3 speed)
    {
        while (Mathf.Abs(speed.x) > 0.01f || Mathf.Abs(speed.y) > 0.01f)
        {
            transform.Translate(-speed);
            speed -= speed * 0.1f * (JudgeMoveSpring(springRange) ? 2 : 1);
            transform.position = MoveLimit(transform.position);
            yield return null;
        }
        #region 旧代码
        //while (Vector3.Distance(transform.position, pos) > 0.1f)
        //{
        //    //Vector3 rhombus = map.gridLayer.TransCoordinate(pos);
        //    //FormatRhombusPos(ref rhombus);
        //    //Vector3 worldPos = map.gridLayer.DeTransCoordinate(rhombus);

        //    //transform.position = Vector3.Lerp(transform.position, pos, 0.1f * throwSpeed);
        //    //if (Vector3.Distance(transform.position, lastTransfromPos) < 0.1f)
        //    //    yield break;
        //    //lastTransfromPos = transform.position;
        //    yield return null;
        //}
        //while(speed)
        //judgeSpring = true;
        #endregion
    }



    private float ScaleLimit(float scale)
    {
        float result = scale;
        if (result < minScale - scaleSpringRange)
            result = minScale - scaleSpringRange;
        else if (result > maxScale + scaleSpringRange)
            result = maxScale + scaleSpringRange;
        return result;
    }


    private Vector3 TransPos(Vector3 pos)
    {
        return Camera.main.ScreenToWorldPoint(pos);
    }


    /// <summary>
    /// 对摄像机的位置进行限制
    /// </summary>
    /// <param name="pos">目标位置</param>
    /// <returns></returns>
    private Vector3 MoveLimit(Vector3 pos/*, LimitType type*/)
    {
        #region 测试代码
        //if (pos.y < map.tileLayer.BottomTile.bottom.y)
        //{
        //    result = new Vector3(result.x, map.tileLayer.BottomTile.bottom.y, result.z);
        //}
        //if (pos.y > map.tileLayer.TopTile.top.y)
        //{
        //    result = new Vector3(result.x, map.tileLayer.TopTile.top.y, result.z);
        //}
        //if (Mathf.Abs(pos.x) > map.tileLayer.RightTile.right.x)
        //{
        //    result = new Vector3(result.x > 0 ? map.tileLayer.RightTile.right.x :
        //        map.tileLayer.LeftTile.left.x, result.y, result.z);
        //}
        //if (Mathf.Abs((result.y + map.diagonal / 2) / result.x) < Mathf.Tan(MapEditor.tileRad))
        //{
        //    result = new Vector3(result.x, result.x < 0 ? -result.x * Mathf.Tan(MapEditor.tileRad) - map.diagonal / 2 :
        //        result.x * Mathf.Tan(MapEditor.tileRad) - map.diagonal / 2, result.z);
        //}
        //if (Mathf.Abs((map.tileLayer[map.tileLayer.Count - 1].top.y - result.y) / result.x) <
        //    Mathf.Tan(MapEditor.tileRad))
        //{
        //    result = new Vector3(result.x, result.x < 0 ? map.tileLayer[map.tileLayer.Count - 1].top.y
        //        + result.x * Mathf.Tan(MapEditor.tileRad) : map.tileLayer[map.tileLayer.Count - 1].top.y
        //        - result.x * Mathf.Tan(MapEditor.tileRad), result.z);
        //}
        #endregion
        #region 测试代码II
        ////为防止地图可能出现N*M的情况 , 分别计算每个顶点和边的限制 
        ////对下顶点进行限制 
        //if (result.y > topTile.y)
        //    result = new Vector3(result.x, topTile.y, result.z);
        ////对上顶点进行限制
        //else if (result.y < bottomTile.y)
        //    result = new Vector3(result.x, bottomTile.y, result.z);

        ////对右顶点进行限制
        //if (result.x > rightTile.x)
        //    result = new Vector3(rightTile.x, result.y, result.z);
        ////对左顶点进行限制 
        //else if (result.x < leftTile.x)
        //    result = new Vector3(leftTile.x, result.y, result.z);



        ////对下面两条边进行限制 由于第一个Tile始终处于菱形坐标系原点位置, 不用做特殊处理
        //if (Mathf.Abs((result.y - bottomTile.y) / result.x) < tangent)
        //{
        //    result = new Vector3(result.x, result.x < 0 ? -result.x * tangent + bottomTile.y :
        //        result.x * tangent + bottomTile.y, result.z);
        //}
        ////对上面两条边进行限制, 由于最后一个Tile的位置受地图尺寸影响, 需要将x的坐标转为相对于
        ////上顶点的位置 
        //else if (Mathf.Abs((topTile.y - result.y) / (result.x - topTile.x)) < tangent)
        //    result = new Vector3(result.x, result.x < topTile.x
        //        ? topTile.y + (result.x - topTile.x) * tangent :
        //        topTile.y - (result.x - topTile.x) * tangent, result.z);

        //needSpring = topNBottom || leftNRight || border;
        #endregion

        Vector3 rhombusPos = map.gridLayer.TransCoordinate(pos);
        if (rhombusPos.x < limitRange)
            rhombusPos.x = limitRange;
        if (rhombusPos.y < limitRange)
            rhombusPos.y = limitRange;
        if (rhombusPos.x > map.gridLayer.Max_X - limitRange)
            rhombusPos.x = map.gridLayer.Max_X - limitRange;
        if (rhombusPos.y > map.gridLayer.Max_Y - limitRange)
            rhombusPos.y = map.gridLayer.Max_Y - limitRange;

        Vector3 result = map.gridLayer.DeTransCoordinate(rhombusPos);
        return result;
    }


    private bool JudgeScaleSpring()
    {
        return CameraSize < minScale ||
            CameraSize > maxScale;
    }

    private bool JudgeMoveSpring(float Range)
    {
        Vector3 rhombusPos = map.gridLayer.TransCoordinate(transform.position);
        return rhombusPos.x < Range || rhombusPos.y < Range ||
            rhombusPos.x > map.gridLayer.Max_X - Range ||
            rhombusPos.y > map.gridLayer.Max_Y - Range;
    }


    private float GetRectDistance()
    {
        Vector3 rhombusPos = map.gridLayer.TransCoordinate(transform.position);
        if (rhombusPos.x < limitRange + springRange)
            return rhombusPos.x;
        if (rhombusPos.y < limitRange + springRange)
            return rhombusPos.y;
        if (rhombusPos.x > map.gridLayer.Max_X - limitRange + springRange)
            return map.gridLayer.Max_X - rhombusPos.x;
        if (rhombusPos.y > map.gridLayer.Max_Y - limitRange + springRange)
            return map.gridLayer.Max_Y - rhombusPos.x;
        return 0;
    }


    private Vector3 FormatRhombusPos(Vector3 pos)
    {
        if (pos.x < limitRange + springRange)
            pos.x = limitRange + springRange;
        if (pos.y < limitRange + springRange)
            pos.y = limitRange + springRange;
        if (pos.x > map.gridLayer.Max_X - limitRange - springRange)
            pos.x = map.gridLayer.Max_X - limitRange - springRange;
        if (pos.y > map.gridLayer.Max_Y - limitRange - springRange)
            pos.y = map.gridLayer.Max_Y - limitRange - springRange;
        return pos;
    }


    private void SpringMove()
    {
        beginJudgeMoveSpring = JudgeMoveSpring(springRange - 0.1f);

        Vector3 rhombusPos = map.gridLayer.TransCoordinate(transform.position);
        rhombusPos = FormatRhombusPos(rhombusPos);

        Vector3 pos = Vector3.Lerp(map.gridLayer.TransCoordinate(transform.position),
            rhombusPos, 0.1f);

        Vector3 worldPos = map.gridLayer.DeTransCoordinate(pos);

        transform.position = worldPos;

    }


    private void SpringScale()
    {
        beginJudgeScaleSpring = JudgeScaleSpring();
        if (CameraSize < minScale)
            CameraSize += 0.01f * scaleSpeed;
        if (CameraSize > maxScale)
            CameraSize -= 0.01f * scaleSpeed;
    }



    #region 旧代码

    /// <summary>
    /// 判断是否需要进行移动回弹 
    /// </summary>
    /// <returns></returns>
    //private bool JudgeMoveSpring()
    //{

    //    //Vector3 pos = transform.position;


    //    //Vector3 pos2Left = leftTile - pos;
    //    //Vector3 pos2Bottom = bottomTile - pos;
    //    //Vector3 pos2Right = rightTile - pos;
    //    //Vector3 pos2Top = topTile - pos;

    //    ////距离第一象限斜边的距离
    //    //float distance1 = Vector3.Distance(pos, rightTile) * Mathf.Sin(Vector3.Angle(pos2Right, top2Right) * Mathf.Deg2Rad);
    //    ////第四象限
    //    //float distance2 = Vector3.Distance(pos, bottomTile) * Mathf.Sin(Vector3.Angle(pos2Bottom, right2Bottom) * Mathf.Deg2Rad);
    //    ////第三象限
    //    //float distance3 = Vector3.Distance(pos, leftTile) * Mathf.Sin(Vector3.Angle(pos2Left, bottom2Left) * Mathf.Deg2Rad);
    //    ////第二象限
    //    //float distance4 = Vector3.Distance(pos, topTile) * Mathf.Sin(Vector3.Angle(pos2Top, left2Top) * Mathf.Deg2Rad);


    //    //return distance1 < springRange || distance2 < springRange
    //    //    || distance3 < springRange || distance4 < springRange;


    //}
    #endregion



}
