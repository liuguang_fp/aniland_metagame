﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class BinarySerialize 
{



    public void SerializeToFile<T>(string path, T data)
    {
        using (FileStream stream = new FileStream(path, FileMode.Create))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, data);
        }
    }


    public void SerializeToMemory<T>(out byte[] memoryDatas, T data)
    {
        using (MemoryStream stream = new MemoryStream())
        {
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, data);
            memoryDatas = new byte[stream.Length];
            stream.Read(memoryDatas, 0, (int)stream.Length);
            memoryDatas = stream.ToArray();
        }
    }


    public T DeserializeFromFile<T>(string path)
    {
        using (FileStream stream = new FileStream(path, FileMode.Open))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            T data= (T)formatter.Deserialize(stream);
            if (data != null)
                return data;
            return default(T);
        }
    }

    public T DeserializeFromMemory<T>(byte[] datas)
    {
        using(MemoryStream stream = new MemoryStream(datas))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            T data = (T)formatter.Deserialize(stream);

            if (data != null)
                return data;
            return default(T);
        }
    }
}
