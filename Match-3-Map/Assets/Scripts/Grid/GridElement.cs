﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GridElement
{

    public float diagonal;
    //标记菱形区域的边长 
    public float side;

    public int tileLayer;

    public int buildingLayer;

    //中心点位置坐标 
    public Vector3 pivot;

    public Vector3 bottom
    {
        get
        {
            return new Vector3(pivot.x, pivot.y - diagonal / 2/*- side * Mathf.Sin(MapEditor.tileRad)*/, pivot.z);
        }
    }

    public Vector3 top
    {
        get
        {
            return new Vector3(pivot.x, pivot.y + diagonal / 2/*side * Mathf.Sin(MapEditor.tileRad)*/, pivot.z);
        }
    }

    public Vector3 left
    {
        get
        {
            return new Vector3(pivot.x - diagonal /*Mathf.Cos(MapEditor.tileRad)*/, pivot.y, pivot.z);
        }
    }

    public Vector3 right
    {
        get
        {
            return new Vector3(pivot.x + diagonal /*Mathf.Cos(MapEditor.tileRad)*/, pivot.y, pivot.z);
        }
    }

    public bool available;


    public bool hasTile;

    public bool hasBuilding;


    public MapElement tile;

    public MapElement building;


    public GridElement(float _diagonal, float _side, Vector3 _pivot, int tileL,int buildingL)
    {
        diagonal = _diagonal;
        side = _side;
        pivot = _pivot;
        tileLayer = tileL;
        buildingLayer = buildingL;

    }


    public void ChangeAvailable(bool state)
    {
        available = state;
    }

}
