﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
#if UNITY_EDITOR
using UnityEditor.SceneManagement;
#endif

[Serializable]
public class GridLayer : IEnumerable
{
    /// <summary>
    /// 菱形坐标系象限 
    /// </summary>
    public enum RhombusQuadrant
    {
        None,
        I,
        II,
        III,
        IV
    }

    public int col;

    public int row;

    public float diagonal;

    public float side;

    public List<GridElement> datas;

    /// <summary>
    /// 区域Grid总数
    /// </summary>
    public int Count
    {
        get
        {
            return datas.Count;
        }
    }
    /// <summary>
    /// 区域上顶点Tile
    /// </summary>
    public GridElement TopTile
    {
        get
        {
            return datas[Count - 1];
        }
    }
    /// <summary>
    /// 区域下顶点Tile
    /// </summary>
    public GridElement BottomTile
    {
        get
        {
            return datas[0];
        }
    }
    /// <summary>
    /// 区域左顶点Tile
    /// </summary>
    public GridElement LeftTile
    {
        get
        {
            return datas[Count - col];
        }
    }
    /// <summary>
    /// 区域右顶点Tile
    /// </summary>
    public GridElement RightTile
    {
        get
        {
            return datas[col - 1];
        }
    }

    public float Max_Y
    {
        get
        {
            return side * row;
        }
    }

    public float Max_X
    {
        get
        {
            return side * col;
        }
    }

    public GridLayer(int _col, int _row, float _side, float _diagonal)
    {
        col = _col;
        row = _row;
        side = _side;
        diagonal = _diagonal;
        datas = new List<GridElement>(col * row);
    }


    public GridElement this[int index]
    {
        get
        {
            return datas[index];
        }
    }

    /// <summary>
    /// 将Grid对象添加进数据集合 
    /// </summary>
    /// <param name="element"></param>
    public void AddGridElement(GridElement element)
    {
        datas.Add(element);
    }
#if UNITY_EDITOR
    /// <summary>
    /// 在编辑器生成格子数据
    /// </summary>
    public void CreateGridLayer()
    {
        int count = col * row;
        Transform content = GameObject.Find("TileContent").transform;
        List<GameObject> tiles = new List<GameObject>();
        for (int i = 0; i < content.childCount; i++)
        {
            tiles.Add(content.GetChild(i).gameObject);
        }
        for (int i = 0; i < tiles.Count; i++)
        {
            GameObject.DestroyImmediate(tiles[i]);
        }
        tiles = null;
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < col; j++)
            {
                Vector3 pos = new Vector3();
                pos.x = j * diagonal /*size * Mathf.Cos(MapEditor.tileRad) */- i * diagonal /*size * Mathf.Cos(MapEditor.tileRad)*/;
                pos.y = j * diagonal / 2 /*size * Mathf.Sin(MapEditor.tileRad)*/ + i * diagonal / 2/*size * Mathf.Sin(MapEditor.tileRad)*/- diagonal / 2 * (row - 1);
                int layer = count - (i * col + j);
                GridElement data = new GridElement(diagonal, side, pos, layer, layer + count);
                datas.Add(data);
            }
        }
        EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
    }

#endif

    public int GetGridIndexByPos(Vector3 pos)
    {
        Vector3 target = TransCoordinate(pos);
        DeTransCoordinate(target);
        if (target.x < 0 || target.y < 0 || target.x > Max_X || target.y > Max_Y)
        {
            //Debug.LogError("当前鼠标位置无数据！");
            return -1;
        }
        //根据菱形坐标系中的坐标计算出当前鼠标位置的索引
        int index = (int)(target.y / side) * col + (int)(target.x / side);
        return index;
    }

    /// <summary>
    /// 通过指定世界位置获得Tile数据 
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    public GridElement GetGridDataByPos(Vector3 pos)
    {
        int index = GetGridIndexByPos(pos);
        if (index == -1)
            return null;
        else
            return datas[index];
    }

    /// <summary>
    /// 将世界坐标转换为当前菱形坐标系坐标
    /// </summary>
    /// <param name="pos">世界坐标系坐标</param>
    /// <returns></returns>
    public Vector3 TransCoordinate(Vector3 pos)
    {
        Vector3 zero = BottomTile.bottom;//菱形坐标原点
        Vector3 posToZero = pos - zero;//由原点指向目标位置的向量 
        float disForPosToZero = Vector3.Distance(zero, pos);//原点指向目标位置的距离
        Vector3 axis_x = RightTile.right - zero;//菱形坐标X轴向量
        float angle = Vector3.Angle(posToZero, axis_x);//目标向量与X轴的夹角
        float disForPosToAxisX = disForPosToZero * Mathf.Sin(angle * Mathf.Deg2Rad);//目标位置到X轴的距离
        float value_y = disForPosToAxisX / Mathf.Sin(MapEditor.tileRad * 2);//转换后菱形坐标Y轴分量
        //确定Y轴分量的符号 
        if (Mathf.Abs(posToZero.y / posToZero.x) < Mathf.Tan(MapEditor.tileRad))
        {
            if (posToZero.x > 0)
                value_y = -value_y;
        }
        else
        {
            if (posToZero.y < 0)
                value_y = -value_y;
        }
        float value_x = disForPosToZero * Mathf.Cos(angle * Mathf.Deg2Rad) +
        value_y * Mathf.Cos(MapEditor.tileRad * 2);//转换后菱形坐标X轴分量 


        Vector3 result = new Vector3(value_x, value_y, 0);

        return result;
    }

    /// <summary>
    /// 将当前菱形坐标系坐标还原回世界坐标系
    /// </summary>
    /// <param name="pos">菱形坐标系坐标</param>
    /// <returns></returns>
    public Vector3 DeTransCoordinate(Vector3 pos)
    {
        float disForPosToAxisX = pos.y * Mathf.Sin(MapEditor.tileRad * 2); // 反推点到菱形X轴距离 
        float disForAxisX = pos.x - pos.y * Mathf.Cos(MapEditor.tileRad * 2);// 点到其再X轴上投影点的距离
        float angle = Mathf.Atan(disForPosToAxisX / disForAxisX) * Mathf.Rad2Deg; // 点与X轴夹角
        float disForPosToZero = Mathf.Sqrt(Mathf.Pow(disForAxisX, 2) + Mathf.Pow(disForPosToAxisX, 2));//点到菱形坐标原点距离

        float gridAngle = 90 - MapEditor.tileRad * Mathf.Rad2Deg; //菱形钝角的角度

        float realAngle; //换算之后点到菱形坐标原点与世界Y轴的夹角

        //格式化角度
        if (angle < 0)
            angle = 180 + angle;
        if (angle < gridAngle)
            realAngle = gridAngle - angle;
        else if (angle < gridAngle * 2)
            realAngle = angle - gridAngle;
        else
        {
            if (angle < gridAngle * 2 + MapEditor.tileRad * Mathf.Rad2Deg)
                realAngle = angle - gridAngle;
            else
                realAngle = 180 + gridAngle - angle;
        }

        //计算得到点在世界坐标轴中的分量
        float value_x = disForPosToZero * Mathf.Sin(realAngle * Mathf.Deg2Rad);
        float value_y = disForPosToZero * Mathf.Cos(realAngle * Mathf.Deg2Rad);


        if (float.IsNaN(value_x))
            value_x = 0;
        if (float.IsNaN(value_y))
            value_y = 0;


        //确定分量符号
        switch (JudgeQuadrant(pos))
        {
            case RhombusQuadrant.II:
                if (angle > gridAngle * 2 + MapEditor.tileRad * Mathf.Rad2Deg)
                    value_y = -value_y;
                break;
            case RhombusQuadrant.III:
                value_y = -value_y;
                break;
            case RhombusQuadrant.IV:
                if (angle < gridAngle * 2 + MapEditor.tileRad * Mathf.Rad2Deg)
                    value_y = -value_y;
                break;
        }
        //世界坐标X轴在菱形坐标系中的表示
        Vector3 axis_Vertical = (TransCoordinate(TopTile.top) - TransCoordinate(BottomTile.bottom)).normalized;

        Vector3 pos_Vertical = (pos - TransCoordinate(BottomTile.bottom)).normalized;

        //确定分量符号
        value_x = Mathf.Abs(pos_Vertical.y / pos_Vertical.x) < Mathf.Abs(axis_Vertical.y / axis_Vertical.x) ? value_x : -value_x;

        Vector3 result = new Vector3(value_x, value_y);

        result += BottomTile.bottom;
        return result;
    }



    /// <summary>
    /// 判断指定位置在当前坐标系中的象限
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    private RhombusQuadrant JudgeQuadrant(Vector3 pos)
    {
        if (pos.x > 0 && pos.y > 0)
            return RhombusQuadrant.I;
        else if (pos.x < 0 && pos.y > 0)
            return RhombusQuadrant.II;
        else if (pos.x < 0 && pos.y < 0)
            return RhombusQuadrant.III;
        else if (pos.y < 0 && pos.x > 0)
            return RhombusQuadrant.IV;
        else return RhombusQuadrant.None;
    }



    public IEnumerator GetEnumerator()
    {
        foreach (var item in datas)
        {
            yield return item;
        }
    }
}
