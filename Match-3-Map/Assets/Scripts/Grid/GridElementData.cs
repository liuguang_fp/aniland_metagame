﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GridElementData
{

    public float position_x;

    public float position_y;

    public float position_z;

    public bool available;

    public int tileLayer;

    public int buildingLayer;

    public MapElementData tileData;
    public MapElementData buildingData;


    public GridElementData(Vector3 pivot, bool avail ,int tileL, int buildingL)
    {
        position_x = pivot.x;
        position_y = pivot.y;
        position_z = pivot.z;
        available = avail;
        tileLayer = tileL;
        buildingLayer = buildingL;
    }
}
