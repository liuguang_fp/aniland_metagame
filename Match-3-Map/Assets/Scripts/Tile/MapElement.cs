﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MapElement : MonoBehaviour
{


    public ElementType type;

    public int colSize;
    public int rowSize;

    public MapElementData elementData;


    public GridElement gridData;

    public SpriteRenderer renderer;


    public string assetsPath;



    public void SetLayer(int index)
    {
        renderer.sortingOrder = index;
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawCube(gridData.pivot, new Vector3(0.2f, 0.2f, 0.1f));
    }
}

