﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MapElementData
{

    public string assetsPath;

    //public float position_x;
    //public float position_y;
    //public float position_z;

    public float offset_x;
    public float offset_y;
    public float offset_z;

    public int sortingLayer;



}
