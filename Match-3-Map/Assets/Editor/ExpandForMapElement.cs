﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(MapElement))]
[CanEditMultipleObjects]
[ExecuteInEditMode]
public class ExpandForMapElement : Editor
{
    public MapElement element
    {
        get
        {
            return target as MapElement;
        }
    }

    public bool canDo = true;

    private void OnSceneGUI()
    {
        Event e = Event.current;
        if (e.keyCode == KeyCode.W && canDo)
        {
            element.transform.Translate(0, 0.05f, 0);
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
        if (e.keyCode == KeyCode.S && canDo)
        {
            element.transform.Translate(0, -0.05f, 0);
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }

        if (e.keyCode == KeyCode.A && canDo)
        {
            element.transform.Translate(-0.05f, 0, 0);
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
        if (e.keyCode == KeyCode.D && canDo)
        {
            element.transform.Translate(0.05f, 0, 0);
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
        if (e.keyCode == KeyCode.Q && canDo)
        {
            element.transform.Translate(0, 0, -0.1f);
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
        if (e.keyCode == KeyCode.E && canDo)
        {
            element.transform.Translate(0, 0, 0.1f);
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
        if (e.keyCode == KeyCode.R && canDo)
        {
            element.renderer.sortingOrder += 1;
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }
        if (e.keyCode == KeyCode.T && canDo)
        {
            element.renderer.sortingOrder -= 1;
            canDo = false;
            EditorSceneManager.MarkSceneDirty(EditorSceneManager.GetActiveScene());
        }




        if (e.type == EventType.KeyUp)
            canDo = true;

    }




}
