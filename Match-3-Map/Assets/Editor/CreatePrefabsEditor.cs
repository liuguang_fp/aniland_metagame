﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class CreatePrefabsEditor : MonoBehaviour
{

    const string prefabPath = "Assets/Resources/Prefabs/";

    static int succeedCount;
    static int failedCount;


    [MenuItem("Assets/CreateTilePrefabs")]
    private static void CreateTile()
    {
        CreatePrefabs(ElementType.Tile);
    }
    [MenuItem("Assets/CreateBuildingPrefabs")]
    private static void CreateBuilding()
    {
        CreatePrefabs(ElementType.Building);
    }


    private static void CreatePrefabs(ElementType type)
    {
        Texture[] textures = Selection.GetFiltered<Texture>(SelectionMode.DeepAssets);
        for (int i = 0; i < textures.Length; i++)
        {
            Texture item = textures[i];

            string path = AssetDatabase.GetAssetPath(item);

            string[] info = path.Split('/');

            string tileClass = info[info.Length - 2];

            if (!Directory.Exists(prefabPath + tileClass))
            {
                Directory.CreateDirectory(prefabPath + tileClass);
            }

            GameObject prefab = new GameObject(item.name);
            SpriteRenderer renderer = prefab.AddComponent<SpriteRenderer>();
            MapElement tile = prefab.AddComponent<MapElement>();

            string assetsPath = prefabPath + tileClass + "/" + item.name;

            tile.assetsPath = assetsPath.Replace("Assets/Resources/","");
            tile.renderer = renderer;
            tile.type = type;
            renderer.sprite = AssetDatabase.LoadAssetAtPath<Sprite>(path);
            tile.colSize = 1;
            tile.rowSize = 1;
            switch (type)
            {
                case ElementType.Tile:
                    tile.gameObject.layer = 9;
                    break;
                case ElementType.Building:
                    tile.gameObject.layer =  8;
                    break;
            }
            if (File.Exists(assetsPath + ".prefab"))
            {
                failedCount++;
            }
            else
            {
                succeedCount++;
                PrefabUtility.CreatePrefab(assetsPath + ".prefab", prefab);
            }

            DestroyImmediate(prefab);
            AssetDatabase.Refresh();
        }
        EditorUtility.DisplayDialog("创建完成", string.Format("{0}增加{1}已存在", succeedCount, failedCount), "关闭");
        succeedCount = 0;
        failedCount = 0;
    }



}
