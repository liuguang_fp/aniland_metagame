﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[CanEditMultipleObjects]
[CustomEditor(typeof(MapEditor))]
[ExecuteInEditMode]
public class ExpandForMapEditor : Editor
{


    private int SelectFolder;

    private MapEditor Map { get { return target as MapEditor; } }

    private string classPath = "Assets/Resources/Prefabs";
    //private string prefabsPath = "Assets/Resources/Prefabs";

    public void OnSceneGUI()
    {
        Event e = Event.current;
        if (e.alt)
            Selection.activeGameObject = Map.gameObject;

        if (e.isKey)
        {
            Vector2 pos = HandleUtility.GUIPointToWorldRay(e.mousePosition).origin;
            GridElement data = Map.gridLayer.GetGridDataByPos(pos);
            if (data != null)
            {
                switch (e.keyCode)
                {
                    case KeyCode.Z:
                        data.ChangeAvailable(Map.availableMode == 0);
                        break;
                    case KeyCode.X:
                        Map.CreateTile(data);
                        break;
                    case KeyCode.C:
                        Map.DeleteTile(data, ElementType.Tile);
                        break;
                    case KeyCode.V:
                        Map.DeleteTile(data, ElementType.Building);
                        break;
                }
            }
        }
    }



    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();
        serializedObject.ApplyModifiedPropertiesWithoutUndo();

        DirectoryInfo classDir = new DirectoryInfo(classPath);
        List<string> classNames = new List<string>();
        if (classDir.Exists)
        {
            FileInfo[] files = classDir.GetFiles();
            for (int i = 0; i < files.Length; i++)
            {
                string name = files[i].Name.Replace(".meta", "");
                classNames.Add(name);
            }
        }
        GUILayout.Label("----------选择资源类型-----------");
        SelectFolder = GUILayout.Toolbar(SelectFolder, classNames.ToArray());
        string prefabsPath = "Assets/Resources/Prefabs/" + classNames[SelectFolder];

        DirectoryInfo directory = new DirectoryInfo(prefabsPath);
        List<string> names = new List<string>();
        if (directory.Exists)
        {
            FileInfo[] files = directory.GetFiles();
            for (int i = 0; i < files.Length; i++)
            {
                string name = files[i].Name;
                if (!name.Contains("meta"))
                    names.Add(name);
            }
        }

        Texture texture = MapEditor.CurrentSelectObj == null ? null : MapEditor.CurrentSelectObj.GetComponent<SpriteRenderer>().sprite.texture;
        GUILayout.Label("----------当前选中资源-----------");
        GUILayout.Box(texture);



        GUILayout.Label("----------选择一个资源-----------");
        for (int i = 0; i < names.Count; i++)
        {
            if (i % 3 == 0)
                GUILayout.BeginHorizontal();
            string name = names[i];
            GameObject obj = AssetDatabase.LoadAssetAtPath<GameObject>(prefabsPath + "/" + name);
            Texture tex = obj.GetComponent<SpriteRenderer>().sprite.texture;
            if (GUILayout.Button(tex, GUILayout.Height(70), GUILayout.Width(70)))
                MapEditor.CurrentSelectObj = obj;

            if (i % 3 == 2 || i == names.Count - 1)
                GUILayout.EndHorizontal();
        }

        //GUILayout.BeginHorizontal();


        GUILayout.BeginHorizontal();
        GUILayout.Label("显示格子状态");
        Map.showAvailable = GUILayout.Toggle(Map.showAvailable, "");
        GUILayout.Label("格子编辑模式");
        Map.availableMode = GUILayout.Toolbar(Map.availableMode, new string[] { "可用", "不可用" });
        GUILayout.EndHorizontal();

        if (GUILayout.Button("导出地图数据"))
            Map.SerializeMapData();

        GUILayout.BeginHorizontal();
        GUILayout.Label("列数");
        Map.col = EditorGUILayout.IntField(Map.col);
        GUILayout.Label("行数");
        Map.row = EditorGUILayout.IntField(Map.row);
        GUILayout.Label("对角线");
        Map.diagonal = EditorGUILayout.FloatField(Map.diagonal);
        GUILayout.EndHorizontal();
        if (GUILayout.Button("更新网格"))
        {
            if (EditorUtility.DisplayDialog("确认操作", "更新网格会重置当前所有地图数据, 并删除已摆放的地图元素, 确定继续?", "确认"))
            {
                Map.gridLayer = new GridLayer(Map.col, Map.row, Map.side, Map.diagonal);
                Map.gridLayer.CreateGridLayer();
                HandleUtility.Repaint();
            }
        }
    }
}
